[![codebeat](https://codebeat.co/badges/fbd559f6-30bb-42e7-bd0f-2568c637f104)](https://codebeat.co/projects/github-com-getrebuild-rebuild-master)
[![Codacy](https://api.codacy.com/project/badge/Grade/599a0a3e46f84e6bbc29e8fbe4632860)](https://www.codacy.com/app/getrebuild/rebuild)
[![codecov](https://codecov.io/gh/getrebuild/rebuild/branch/master/graph/badge.svg)](https://codecov.io/gh/getrebuild/rebuild)
[![Build Status](https://travis-ci.org/getrebuild/rebuild.svg?branch=master)](https://travis-ci.org/getrebuild/rebuild)
[![License GPLv3](https://img.shields.io/github/license/getrebuild/rebuild.svg)](https://raw.githubusercontent.com/getrebuild/rebuild/master/LICENSE)
[![License COMMERCIAL](https://img.shields.io/badge/license-COMMERCIAL-orange.svg)](https://raw.githubusercontent.com/getrebuild/rebuild/master/COMMERCIAL)


### 快速开始

- 在线试用（用户名密码均为 admin）https://nightly.getrebuild.com/
- 快速搭建开发环境 https://getrebuild.com/docs/dev/
- 访问 RB 官网了解更多 https://getrebuild.com/


### Quick start

- Trial nightly build (username and password are both `admin`) https://nightly.getrebuild.com/
- Build development environment https://getrebuild.com/docs/dev/
- Visit the RB website to learn more https://getrebuild.com/


### NOTICE

> REBUILD 使用 [开源 GPL-3.0](https://raw.githubusercontent.com/getrebuild/rebuild/master/LICENSE) 和 [商用](https://raw.githubusercontent.com/getrebuild/rebuild/master/COMMERCIAL) 双重授权许可，您应当认真阅读许可内容。使用 REBUILD 即表示您完全同意许可内容/条款。感谢支持！

> REBUILD uses the [open source GPL-3.0](https://raw.githubusercontent.com/getrebuild/rebuild/master/LICENSE) and [commercial](https://raw.githubusercontent.com/getrebuild/rebuild/master/COMMERCIAL) dual license agreements, and you should read the contents of the agreement carefully. By using REBUILD, you fully agree to the Licensed Content/Terms. Thanks for the support!